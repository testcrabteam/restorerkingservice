<?php

  function ParseMysqlResultToArray($result)
  {
    $rows = array();

    if ($result)
    {
      for ($i = 0; $row = $result->fetch_assoc(); $i++) $rows[$i] = $row;
    }

    return $rows;
  }

?>
