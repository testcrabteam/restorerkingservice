<?php

  require_once 'connection.php';
  require_once 'htmlToDatabaseNames.php';

  $link = mysqli_connect(
    $GLOBALS["host"],
    $GLOBALS["user"],
    $GLOBALS["password"],
    $GLOBALS["database"]
  )
      or die ("Error " . mysqli_error($link));

  if (isset($_POST["id"])
  && isset($_POST["statement"])
  && isset($_POST["page"]))
  {
    $id = htmlentities(mysqli_real_escape_string($link, $_POST["id"]));
    $statement = $_POST["statement"];

    $page = htmlentities(mysqli_real_escape_string($link, $_POST["page"]));
    $table = ConvertHtmlPageNameToDatabaseName($page);

    $query = "UPDATE $table SET statement = $statement
              WHERE id = '$id'";
    $result = mysqli_query($link, $query)
        or die("Error " . mysqli_error($link));

    if ($result)
    {
      echo true;
    }
  }

  mysqli_close($link);

?>
