<?php

  require_once 'connection.php';
  require_once 'mysqlSelectHandler.php';

  $link = mysqli_connect(
    $GLOBALS["host"],
    $GLOBALS["user"],
    $GLOBALS["password"],
    $GLOBALS["database"]
  )
      or die ("Error " . mysqli_error($link));

  if (isset($_GET["id"])
  && isset($_GET["idOrder"])
  && isset($_GET["sortField"])
  && isset($_GET["sortCase"]))
  {
    $id = htmlentities(mysqli_real_escape_string($link, $_GET["id"]));
    $idOrder = htmlentities(mysqli_real_escape_string($link, $_GET["idOrder"]));

    $field = $_GET["sortField"];
    $case = $_GET["sortCase"];

    $query = "SELECT task.id, task.idWorker,task.dateComplete, task.description,
              task.statement
              FROM `worker`
              JOIN task
              ON (worker.id = task.idWorker) and (task.idOrder = '$idOrder')
              WHERE idDepartment = '$id'
              ORDER BY $field $case";

    $result = mysqli_query($link, $query)
        or die("Error " . mysqli_error($link));

    $rows = ParseMysqlResultToArray($result);
    $recordsArr = array_values($rows);

    echo json_encode($recordsArr);
  }

  mysqli_close($link);
?>
