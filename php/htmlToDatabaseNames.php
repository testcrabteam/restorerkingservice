<?php

  function ConvertHtmlPageNameToDatabaseName($name)
  {
    $htmlNames = array
    (
      "clientPage",
      "clientList",
      "contacts",
      "department",
      "departmentList",
      "index",
      "login",
      "order",
      "orderForm",
      "task",
      "taskList"
    );

    $databaseNames = array
    (
      "account",
      "account",
      "account",
      "department",
      "department",
      "",
      "account",
      "clientorder",
      "clientorder",
      "task",
      "task"
    );

    $table = NULL;
    if (($index = array_search($name, $htmlNames)) !== false)
    {
      $table = $databaseNames[$index];
    }
    return $table;
  }


?>
