<?php
  require_once 'connection.php';

  $link = mysqli_connect($host, $user, $password, $database)
      or die ("Error " . mysqli_error($link));

  if (isset($_POST["email"])
  && isset($_POST["password"]))
  {
    $email = htmlentities(mysqli_real_escape_string($link, $_POST["email"]));
    $pswd = htmlentities(mysqli_real_escape_string($link, $_POST["password"]));


    $query =  "SELECT * FROM `account`
        WHERE (email = '$email') and (password = '$pswd')";

    $result = mysqli_query($link, $query)
        or die ("Error " . mysqli_error($link));

    if($result)
    {
      $resultObj = $result->fetch_object();

      $query =  "SELECT * FROM `client` WHERE email = '$email'";
      $result = mysqli_query($link, $query)
          or die ("Error " . mysqli_error($link));

      if ($result->num_rows !== 0) $status = "client";
      else $status = "worker";

      $accountInfo = new AccountInfo($resultObj, $status);

      echo json_encode($accountInfo);
    }
  }

  class AccountInfo
  {
    public $account;
    public $status;

    function __construct($account, $status)
    {
      $this->account = $account;
      $this->status = $status;
    }
  }

  mysqli_close($link);
?>
