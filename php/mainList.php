<?php

  require_once 'connection.php';
  require_once 'mysqlSelectHandler.php';

  $link = mysqli_connect($host, $user, $password, $database)
      or die ("Error " . mysqli_error($link));

  if (isset($_GET["page"])
  && isset($_GET["sortField"])
  && isset($_GET["sortCase"]))
  {
    $table = htmlentities(mysqli_real_escape_string($link, $_GET["page"]));
    $field = $_GET["sortField"];
    $case = $_GET["sortCase"];
    switch($table)
    {
      case "ordersList":
        $query = "SELECT * FROM clientorder WHERE 1 ORDER BY $field $case";
      break;
      case "clientsList":
        $query = "SELECT account.email, account.name,
          account.surname, account.middleName, account.regDate
          FROM client
          JOIN account
          ON account.email = client.email
          ORDER BY $field $case";
      break;
      case "departmentsList":
        $query = "SELECT * FROM department WHERE 1 ORDER BY $field $case";
      break;
      default:
      $query = "SELECT * FROM $table WHERE 1 ORDER BY $field $case";
      break;
    }

    $result = mysqli_query($link, $query)
        or die ("Error " . mysqli_error($link));

    $rows = ParseMysqlResultToArray($result);
    $recordsArr = array_values($rows);

    echo json_encode($recordsArr);
  }

  mysqli_close($link);

?>
