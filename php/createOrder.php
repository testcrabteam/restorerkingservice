<?php

  require_once 'connection.php';

  $link = mysqli_connect($host, $user, $password, $database)
      or die("Ошибка " . mysqli_error($link));

  if (isset($_POST["date"])
  && isset($_POST["description"])
  && isset($_POST["idClient"]))
  {
    $date = htmlentities(mysqli_real_escape_string($link, $_POST["date"]));
    $descp = htmlentities(mysqli_real_escape_string($link, $_POST["description"]));
    $idClient = htmlentities(mysqli_real_escape_string($link, $_POST["idClient"]));

    $id = (int)GetLastOrderId($link) + 1;

    $name = "Order" . $id;
    $statement = 1;

    if (isset($_POST["idOrder"]))
    {
      $query = "UPDATE clientorder
                SET description = '$descp', clientDate = '$date'
                WHERE 1";
    }
    else
    {
      $query = "INSERT INTO `clientorder`
      (`idClient`, `name`, `description`, `statement`, `clientDate`)
      VALUES ('$idClient','$name','$descp', '$statement','$date')";
    }

    $result = mysqli_query($link, $query)
        or die ("Error " . mysqli_error($link));

    if ($result)
    {
      echo $id;    //Return id on ajax response
    }
  }

  function GetLastOrderId($link)
  {
    $query = "SELECT MAX(id) AS id FROM clientorder WHERE 1";
    $result = mysqli_query($link, $query)
        or die ("Error " . mysqli_error($link));

    $recordsArr = $result->fetch_assoc();
    $maxId = ($result) ? $recordsArr["id"] : 0;
    return $maxId;
  }

  mysqli_close($link);

?>
