<?php

  require_once 'connection.php';
  require_once 'mysqlSelectHandler.php';

  if (isset($_GET["joinObject"])
  && isset($_GET["sortField"])
  && isset($_GET["sortCase"]))
  {
    $joinObject = json_decode($_GET["joinObject"]);
    $sortConfig = array
    (
      'field' => $_GET["sortField"],
      'case' => $_GET["sortCase"]
    );

    $joinRequestCreator =
        new JoinRequestCreator($joinObject->source, $joinObject->destination);

    $result = $joinRequestCreator
              ->GetJoinQueryRecords($joinObject->value, $sortConfig);
    $rows = ParseMysqlResultToArray($result);
    $recordsArr = array_values($rows);

    echo json_encode($recordsArr);
  }

  class JoinRequestCreator
  {
    private $source;
    private $destination;
    private $destinationAttribute;
    private $sourceAttribute;

    function __construct($source, $destination)
    {
      $this->source = $this->
      ConvertHtmlPageNameToDatabaseTableName($source);
      $this->destination = $this->
      ConvertHtmlPageNameToDatabaseTableName($destination);
      $this->destinationAttribute = $this->
      ConvertHtmlPageNameToDatabaseTableAttributeDestination($source);
      if($this->destinationAttribute === "idOrder")
      {
        $this->sourceAttribute = "idDepartment";
      }
      else
      {
        $this->sourceAttribute = "idOrder";
      }
    }

    function ConvertHtmlPageNameToDatabaseTableName($name)
    {
      $databaseNames = array
      (
        "client",
        "clientorder",
        "department",
        "ordertodepartment",
        "task"
      );

      $table = $this->
      ConvertHtmlPageNameToDatabaseTablePattern($databaseNames, $name);
      return $table;
    }

    function ConvertHtmlPageNameToDatabaseTableAttributeDestination($name)
    {
      $databaseAttributes = array
      (
        "idClient",
        "idOrder",
        "idDepartment",
        "idOrder",
        "idOrder"
      );
      $table = $this->
      ConvertHtmlPageNameToDatabaseTablePattern($databaseAttributes, $name);
      return $table;
    }

    function ConvertHtmlPageNameToDatabaseTablePattern($databasePattern, $name)
    {
      $htmlNames = array
      (
        "client",
        "order",
        "department",
        "ordersWithDepartments",
        "task"
      );

      $table = NULL;
      if (($index = array_search($name, $htmlNames)) !== false)
      {
        $table = $databasePattern[$index];
      }
      return $table;
    }

    public function GetJoinQueryRecords($value, $sortConfig)
    {
      $link = mysqli_connect(
        $GLOBALS["host"],
        $GLOBALS["user"],
        $GLOBALS["password"],
        $GLOBALS["database"]
      )
          or die ("Error " . mysqli_error($link));

      $source = $this->source;
      $destination = $this->destination;
      $destinationAttr = $this->destinationAttribute;
      $sourceAttr = $this->sourceAttribute;
      $field = $sortConfig["field"];
      $case = $sortConfig["case"];

      $query = "SELECT *
          FROM $destination
          JOIN $source
          ON $source.id = $destination.$destinationAttr
          WHERE $destination.$sourceAttr = '$value'
          ORDER BY $field $case";

      $result = mysqli_query($link, $query)
          or die ("Error " . mysqli_error($link));

      mysqli_close($link);
      return $result;
    }
  }

?>
