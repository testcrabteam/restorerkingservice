<?php

  require_once 'connection.php';
  require_once 'mysqlSelectHandler.php';
  require_once 'htmlToDatabaseNames.php';

  $link = mysqli_connect(
    $GLOBALS["host"],
    $GLOBALS["user"],
    $GLOBALS["password"],
    $GLOBALS["database"]
  )
      or die ("Error " . mysqli_error($link));

  if (isset($_GET["id"]) && isset($_GET["page"]))
  {
    $id = htmlentities(mysqli_real_escape_string($link, $_GET["id"]));

    $page = htmlentities(mysqli_real_escape_string($link, $_GET["page"]));
    $table = ConvertHtmlPageNameToDatabaseName($page);
    $attrName = ($table === "account") ? "email" : "id";

    $query = "SELECT * FROM $table WHERE $attrName = '$id'";
    $result = mysqli_query($link, $query)
        or die("Error " . mysqli_error($link));

    //Fix conficts with email and idClient
    if ($table === "account")
    {
      //Conflict on client id in param
      if($result->num_rows === 0)
      {
        $query = "SELECT * FROM client
        JOIN account
        ON account.email = client.email
        WHERE id = '$id'";
      }
      else    //Conflict on client id in email param
      {
        $query = "SELECT * FROM client
        JOIN account
        ON account.email = client.email
        WHERE client.email = '$id'";
      }
    }

    $result = mysqli_query($link, $query)
        or die("Error " . mysqli_error($link));

    $recordsObj = $result->fetch_object();
    echo json_encode($recordsObj);
  }

  mysqli_close($link);

?>
