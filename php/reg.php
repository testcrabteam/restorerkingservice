<?php

  require_once 'connection.php';

  $link = mysqli_connect($host, $user, $password, $database)
      or die("Ошибка " . mysqli_error($link));

  if (isset($_POST["name"])
  && isset($_POST["email"])
  && isset($_POST["password"]))
  {
    $fullName = explode(" ", $_POST["name"]);

    $surname = htmlentities(mysqli_real_escape_string($link, $fullName[0]));
    $name = htmlentities(mysqli_real_escape_string($link, $fullName[1]));
    $middleName = htmlentities(mysqli_real_escape_string($link, $fullName[2]));

    $email = htmlentities(mysqli_real_escape_string($link, $_POST["email"]));
    $pswd = htmlentities(mysqli_real_escape_string($link, $_POST["password"]));
    $regDate = (new DateTime())->format("Y-m-d H:i:s");

    $query =  "INSERT INTO `account`
        (`email`, `name`, `surname`, `middleName`, `password`, `regDate`)
        VALUES ('$email', '$name', '$surname', '$middleName', '$pswd', '$regDate')";

    $result = mysqli_query($link, $query)
        or die ("Error " . mysqli_error($link));

    //Add equal record to client table

    $query =  "INSERT INTO `client`(`email`) VALUES ('$email')";

    $result = mysqli_query($link, $query)
        or die ("Error " . mysqli_error($link));

    if ($result)
    {
      echo $email;    //Return email on ajax response
    }
  }

  mysqli_close($link);

?>
