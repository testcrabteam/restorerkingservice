function DateTimeRenderInRealTime(parent, updateTime)
{
  if (parent != undefined)
  {
    DateTimeRender(parent);
    setInterval(() => DateTimeRender(parent), updateTime);
  }
}

function DateTimeRender(parent)
{
  if (parent != undefined)
  {
    var date = new Date();
    var days =
    [
      "Воскресенье",
      "Понедельник",
      "Вторник",
      "Среда",
      "Четверг",
      "Пятница",
      "Суббота"
    ];
    var dateFormat = date.getHours() + "." + date.getMinutes()
    + "." + date.getSeconds() + ", " + days[date.getDay()];
    parent.textContent = "Сегодня "+ dateFormat;
  }
}

function GetDocumentName()
{
  var documentLocation = document.location.href;
  var documentLocationSplit = documentLocation.split("/");
  var documentName = documentLocationSplit[documentLocationSplit.length - 1];
  var pageName = documentName.split(".")[0];
  return pageName;
}

function CreateHeaderMenu()
{
  var buttons =
  [
    new MenuButton("Главная", "index.html"),
    new MenuButton("Контакты", "contacts.html"),
    new MenuButton("Войти", "login.html")
  ];
  var status = sessionStorage.getItem("status");
  switch(status)
  {
    case "client":
      buttons.push(new MenuButton("Профиль", "clientPage.html"));
    break;
    case "worker":
      buttons.push(new MenuButton("Список заказов", "ordersList.html"));
      buttons.push(new MenuButton("Список клиентов", "clientsList.html"));
      buttons.push(new MenuButton("Список отделов", "departmentsList.html"));
    break;
    default:
    break;
  }
  var menuParent = document.getElementById("header-top");
  var menu = new NavigationMenu(menuParent, buttons);
  menu.Render();
}

class NavigationMenu
{
  constructor(parent, buttons)
  {
    this.parent = parent;
    this.buttons = buttons;

    this.Render = function()
    {
      var root = document.createElement("nav");
      var list = document.createElement("ul");

      CreateButtonsList(buttons, list);

      root.appendChild(list);
      var removeEl = document.querySelector("nav");
      parent.removeChild(removeEl);
      parent.appendChild(root);
    }

    var CreateButtonsList = function(buttons, list)
    {
      for (var i = 0; i < buttons.length; i++)
      {
        var button = buttons[i];
        var htmlButton = button.GetHtmlElement();

        var element = document.createElement("li");

        element.appendChild(htmlButton);
        list.appendChild(element);
      }
    }
  }
}

class MenuButton
{
  constructor(name, href)
  {
    this.name = name;
    this.href = href;

    this.GetHtmlElement = function()
    {
      var a = document.createElement("a");
      a.setAttribute("href", this.href);
      a.textContent = this.name;
      return a;
    }
  }
}

function SetPageTitle()
{
  document.title = "Restorer King";
}

document.addEventListener("DOMContentLoaded", function()
{
  var dateBlock = document.querySelector("#header-logo .dateTime span");
  var documentName = GetDocumentName();
  sessionStorage.setItem("page", documentName);
  // sessionStorage.setItem("clientId", "misterclass2909@yandex.ru");
  // sessionStorage.setItem("idClient", "1");
  // sessionStorage.setItem("status", "worker");
  DateTimeRenderInRealTime(dateBlock, 1000);

  CreateHeaderMenu();
  SetPageTitle();
})
