//Use only with
//linksLibrary.js

function SetLinkText()
{
  var links = document.querySelectorAll(".serverTable td > a");
  links.forEach(function(item)
  {
    item.addEventListener("click", function(e)
    {
      e.preventDefault();

      var link = e.currentTarget.getAttribute("href");
      var linkText = e.currentTarget.textContent;
      sessionStorage.setItem("id", linkText)
      SetUrlLocation(link);
    })
  })
}
