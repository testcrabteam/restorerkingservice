
function SetSessionInfoFromDatabase()
{
  var page = sessionStorage.getItem("page");
  var id = sessionStorage.getItem("id");

  var jqXHR = $.get
  (
    "php/selectQueryCreator.php",
    {'id': id, 'page': page}
  );

  jqXHR.done(function(data)
  {
    var info = JSON.parse(data);
    if (info != undefined)
    {
      sessionStorage.setItem("statement", info.statement);
      sessionStorage.setItem("description", info.description);
      if (page === "order")
      {
        sessionStorage.setItem("date", info.clientDate);
      }
      else sessionStorage.setItem("date", info.dateComplete);
    }
  })
}

function GetInfoFromSession()
{
  var id = sessionStorage.getItem("id");
  var statement = sessionStorage.getItem("statement");
  var statementRus;
  switch(statement)
  {
    case "on preparation":
      statementRus = "На одобрении";
    break;
    case "execute":
      statementRus = "Выполняется";
    break;
    case "completed":
      statementRus = "Выполнен";
    break;
    default:
    statementRus = "Неизвестен";
    break;
  }
  var description = sessionStorage.getItem("description");
  var date = sessionStorage.getItem("date");
  var name = sessionStorage.getItem("nameDescription");

  if (id != undefined)
  {
    var parent = document.getElementById("orderDesc");
    var idBlock = parent.querySelector("h1");
    idBlock.textContent = name + " №" + id;
    var statementBlock = parent.querySelector("h2");
    statementBlock.textContent = "Состояние: " + statementRus;
    var descBlock = parent.querySelector("p");
    descBlock.textContent = description;
    // var dateBlock = document.querySelector("#rootCalendar");
    // dateBlock.textContent = date;
  }
}

function SetSelectValueFromSession(select)
{
  var statement = sessionStorage.getItem("statement");
  var page = sessionStorage.getItem("page");
  var value;
  switch(statement)
  {
    case "on preparation":
      value = 1;
    break;
    case "execute":
      value = (page === "task") ? 1 : 2;
    break;
    case "completed":
      value = (page === "task") ? 2 : 3;
    break;
    default:
      value = 1;
    break;
  }
  select.value = value;
}

function SetElementStatementBySelectValue(select)
{
  var value = select.options[select.selectedIndex].text;
  var statement = document.querySelector("#orderDesc > h2");
  var textBase = "Состояние:"
  statement.textContent = textBase;

  var statementBlock = document.createElement("b");
  statementBlock.textContent = value;
  statement.appendChild(statementBlock);

  SetElementStatementStyleBySelectValue(value);
}

function SetElementStatementStyleBySelectValue(selectTextValue)
{
  var statementStyleBlock = document.querySelector("#orderDesc > h2 > b");
  switch(selectTextValue)
  {
    case "На одобрении":
      statementStyleBlock.style.color = "red";
    break;
    case "Выполняется":
      statementStyleBlock.style.color = "blue";
    break;
    case "Выполнен":
      statementStyleBlock.style.color = "green";
    break;
    default: break;
  }
}

function UpdateDatabaseBySelectValue(select)
{
  var page = sessionStorage.getItem("page");
  var id = sessionStorage.getItem("id");
  var statement = select.value;

  var jqXHR = $.post
  (
    "php/statementUpdate.php",
    {'id': id, 'statement':statement, 'page': page}
  );

  jqXHR.done(function(data)
  {
    if (data) alert("Successful");
  })
}

document.addEventListener("DOMContentLoaded", function()
{
  SetSessionInfoFromDatabase();
  GetInfoFromSession();
  setTimeout(function()
  {
    var select = document.querySelector('select[name = "descStatement"]');
    if (select != undefined)
    {
      SetSelectValueFromSession(select);
      SetElementStatementBySelectValue(select);
      select.addEventListener("change", function()
      {
        SetElementStatementBySelectValue(select);
        UpdateDatabaseBySelectValue(select);
      })
    }
  }, 400)
})
