function HandlingClientEdition()
{
  var savedOrderInfo = JSON.parse(sessionStorage.getItem("orderInfo"));
  if (savedOrderInfo != undefined)
  {
    sessionStorage.setItem("idOrder", savedOrderInfo.id);

    var form = document.getElementById("orderForm");
    var formDate = form.date;
    var formText = form.description;

    var orderDate = new Date(savedOrderInfo.date);
    formDate.value = GetInputFormatDate(orderDate);
    formText.value = savedOrderInfo.text;
  }

  function GetInputFormatDate(date)
  {
    var localeDate = date.toLocaleDateString();
    var dateValues = localeDate.split(".")
    var dateFormat = dateValues[2] + "-" + dateValues[1] + "-"
                     + dateValues[0];
    return dateFormat;
  }
}

function HandleSubmitInForm(form)
{
  var date = form.querySelector('input[name = "date"]').value;
  var descp = form.querySelector('textarea[name = "description"]').value;
  var id = sessionStorage.getItem("idClient");
  var idOrder = sessionStorage.getItem("idOrder");

  var orderObj =
  {
    date: date,
    description: descp
  };
  if (idOrder != undefined)
  {
    var jqXHR = $.post
    (
      "php/createOrder.php",
      {'date': date, 'description': descp, "idClient": id, "idOrder": idOrder}
    )
  }
  else
  {
    var jqXHR = $.post
    (
      "php/createOrder.php",
      {'date': date, 'description': descp, "idClient": id}
    )
  }

  jqXHR.done(function(data)
  {
    sessionStorage.removeItem("idOrder");
    SetUrlLocation("clientPage.html");
  })
}

document.addEventListener("DOMContentLoaded", function()
{
  HandlingClientEdition();

  var form = document.getElementById("orderForm");
  form.addEventListener("submit", function(e)
  {
    e.preventDefault();
    HandleSubmitInForm(form);
  })
})
