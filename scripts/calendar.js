// Use in page ONLY width
// render.js!!

function DateInfo(day, month, year)
{
    this.day = day;
    this.month = month;
    this.year = year;
}

function HtmlCalendar(parent, dateInfo)
{
  var currentDate = new Date();
  this.parent = parent;
  this.currentElement;
  this.calendarContentDays;
  this.calendarHeaderMonths;
  this.calendarHeaderYears;
  this.currentRow = 0;
  this.expectedDay = dateInfo.day;
  this.expectedMonth = dateInfo.month;
  this.expectedYear = dateInfo.year;
  this.currentDay = currentDate.getDate();
  this.currentMonth = currentDate.getMonth() + 1;
  this.currentYear = currentDate.getFullYear();
  this.UpdateEvents;
  const MAX_DAYS_IN_WEEK = 7;
  var eventThis = this;
  this.render = function()
  {
    if (parent != undefined)
    {
      var calendar = CreateDivWithClass("calendar");
      var calendarHeader = this.CreateHeader();
      var calendarText = this.CreateCalendarText();
      var calendarContent = this.CreateCalendarContent();
      calendar.appendChild(calendarHeader);
      calendar.appendChild(calendarText);
      calendar.appendChild(calendarContent);
      var oldContent = parent.querySelector(".calendar");
      if (oldContent != undefined) parent.replaceChild(calendar, oldContent);
      else parent.appendChild(calendar);
      this.UpdateCalendarContent();
    }
  }

  this.UpdateCurrentDayElement = function(date)
  {
    this.currentElement = date.getDay() + this.currentRow * MAX_DAYS_IN_WEEK;
  }

  this.CreateHeader = function()
  {
    var calendarHeader = CreateDivWithClass("calendar-header");
    var months = ["January", "February", "March", "April", "May", "June", "July",
                  "August", "September", "October", "November", "December"];
    var years = ["2001", "2002", "2003", "2004", "2005", "2006", "2007",
                  "2008", "2009", "2010", "2011", "2012", "2013", "2014",
                  "2015", "2016", "2017", "2018", "2019"];
    var calendarMonths = this
                        .CreateHeaderSelection
                        (months, "calendarMonth", this.currentMonth);
    this.calendarHeaderMonths = calendarMonths;
    calendarHeader.appendChild(calendarMonths);
    var calendarYears = this
                        .CreateHeaderSelection
                        (years, "calendarYear", this.currentYear % 1000);
    this.calendarHeaderYears = calendarYears;
    calendarHeader.appendChild(calendarYears);
    return calendarHeader;
  }

  this.CreateHeaderSelection = function(textContents, selectId, value)
  {
    var selection = CreateSelectWithOptionsAndContents(textContents);
    AddIdToElement(selection, selectId);
    AddNameToElement(selection, selectId);
    AddClassToElement(selection, "calendar-select");
    selection.value = value;
    return selection;
  }

  this.CreateCalendarText = function()
  {
    var calendarText = CreateDivWithClass("calendar-text");
    var days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
    var calendarTextDays = CreateUlWithNListsAndContents(days);
    calendarText.appendChild(calendarTextDays);
    return calendarText;
  }

  this.CreateCalendarContent = function()
  {
    var calendarContent = CreateDivWithClass("calendar-content");
    var date = this.CreateCurrentDate();
    this.UpdateCurrentRow();
    this.UpdateCurrentDayElement(date);
    var daysClasses = this.CreateDaysClassArrayForCurrentDate(date);
    var calendarContentDays = CreateUlWithNListsAndContentsWidthClasses(daysClasses.days, daysClasses.classes);
    this.calendarContentDays = calendarContentDays;
    this.calendarContentDays.childNodes.forEach(function(item, i)
    {
      item.addEventListener("click", function()
      {
        if(item.className !== "calendar-other-month-day")
        {
          eventThis.currentDay = StrValueToInt(item.textContent);
          var date = eventThis.CreateCurrentDate();
          eventThis.UpdateCurrentRow();
          eventThis.UpdateCurrentDayElement(date);
          eventThis.UpdateCalendarContent();
          UpdateEventsCall();
        }
      })
    })
    this.RenderActiveDay();
    this.RenderExpectedDay();
    calendarContent.appendChild(calendarContentDays);
    return calendarContent;
  }

  this.CreateCurrentDate = function()
  {
    var date = new Date();
    date.setYear(this.currentYear);
    date.setMonth(this.currentMonth - 1);
    date.setDate(this.currentDay);
    return date;
  }


  this.CreateDaysClassArrayForCurrentDate = function(currentDate)
  {
    var daysClasses =
    {
      days: [],
      classes: []
    }
    var i = 0;
    var length = 42;
    var firstDayOfMonthNumber = GetFirstDayOfMonthNumber(currentDate);
    for (; i < firstDayOfMonthNumber; i++)
    {
      daysClasses.days[i] = GetDaysInMonth(currentDate.getFullYear(), currentDate.getMonth() - 1) - firstDayOfMonthNumber + i + 1;
      daysClasses.classes[i] = "calendar-other-month-day";
    }
    for (var j = 1;
      currentDate.getMonth() == this.currentMonth - 1;
      currentDate.setDate(++j), i++)
    {
      daysClasses.days[i] = j;
      daysClasses.classes[i] = null;
    }
    for (var j = 1; i < length; i++, j++)
    {
      daysClasses.days[i] = j;
      daysClasses.classes[i] = "calendar-other-month-day";
    }
    return daysClasses;
  }

  this.UpdateCurrentRow = function()
  {
    var currentDate = this.CreateCurrentDate();
    var firstDayOfMonthNumber = GetFirstDayOfMonthNumber(currentDate);
    var infelicity = 0.01;
    this.currentRow =  Math.floor
    ((parseInt(this.currentDay) + firstDayOfMonthNumber) / MAX_DAYS_IN_WEEK - infelicity);
  }

  this.RenderActiveDay = function()
  {
    this.GetCurrentHtmlDay().classList.add("calendar-current-day");
  }

  this.GetCurrentHtmlDay = function()
  {
    return this.calendarContentDays.childNodes[this.currentElement];
  }

  this.RenderExpectedDay = function()
  {
    if (this.currentMonth == this.expectedMonth
       && this.currentYear == this.expectedYear)
    {
      var days = this.calendarContentDays
                 .querySelectorAll("li:not(.calendar-other-month-day)");
      var expectedDay = days[this.expectedDay - 1];
      expectedDay.classList.add("calendarExpectedDay");
    }
  }

  this.UpdateCalendarContent = function()
  {
    var monthValues = this.parent.querySelectorAll('#calendarMonth > option');
    var currentMonthValue;
    var yearValues = this.parent.querySelectorAll('#calendarYear > option');
    var currentYearValue;
    for(var i = 0; i < monthValues.length; i++)
    {
      var val = monthValues[i];
      if (val.selected === true) currentMonthValue = val.value;
    }
    for(var i = 0; i < yearValues.length; i++)
    {
      var val = yearValues[i];
      if (val.selected === true) currentYearValue = val.value;
    }

    this.calendarHeaderMonths.addEventListener("change", function()
    {
      eventThis.UpdateCalendarContent();
      UpdateEventsCall();
    });
    this.calendarHeaderYears.addEventListener("change", function()
    {
      eventThis.UpdateCalendarContent();
      UpdateEventsCall();
    })
    this.currentDay = this.GetCurrentHtmlDay().textContent;
    this.currentMonth = currentMonthValue;
    this.currentYear = 2000 + StrValueToInt(currentYearValue);
    var newContent = this.CreateCalendarContent();
    var oldContent = this.parent.querySelector(".calendar-content");
    this.parent.querySelector(".calendar").replaceChild(newContent, oldContent);
  }

  function UpdateEventsCall()
  {
    if (eventThis.UpdateEvents != undefined) eventThis.UpdateEvents();
  }

  function StrValueToInt(str)
  {
    return str - 0;
  }

  function GetDaysInMonth(fullYear, month)
  {
    return 33 - new Date(fullYear, month, 33).getDate();
  }

  function GetFirstDayOfMonthNumber(date)
  {
    var currentDateClone = new Date(date.getFullYear(), date.getMonth(), 1);
    var firstDayOfMonthNumber = currentDateClone.getDay();
    return firstDayOfMonthNumber;
  }
}

function FormHtmlCalendar(calendar, dateInput)
{
  if (calendar instanceof HtmlCalendar && dateInput != undefined)
  {
    this.calendar = calendar;
    this.input = dateInput;
    var calendar = this.calendar;
    var eventThis = this;

    this.Update = function()
    {
      calendar.UpdateCalendarContent();
      eventThis.input.value = calendar.currentMonth
      + "/" + calendar.expectedDay + "/" + calendar.currentYear;
    }

    this.Render = function()
    {
      calendar.render();
      eventThis.Update();
      calendar.UpdateEvents = eventThis.Update;
    }
  }
}

//Create calendar:
// var calendarParent = document.querySelector("parent");
// var calendar = new HtmlCalendar(calendarParent);
// var dateInput = document.querySelector('bindInput');
// var formCalendar = new FormHtmlCalendar(calendar, dateInput);
// formCalendar.Render();
