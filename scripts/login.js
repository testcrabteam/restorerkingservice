function HandleSubmitInForm(form)
{
  var email = form.querySelector('input[name = "email"]').value;
  var password = form.querySelector('input[name = "password"]').value;
  var localClient =
  {
    email: email,
    password: password
  };

  var jqXHR = $.post
  (
    "php/login.php",
    {'email': email, 'password': password}
  );

  jqXHR.done(function(response)
  {
    var responseJson = JSON.parse(response);
    var remoteClient = responseJson.account;

    if (remoteClient != undefined
        && EqualsClientsObjects(localClient, remoteClient))
    {
      sessionStorage.setItem("clientId", localClient.email);
      sessionStorage.setItem("status", responseJson.status);
      if (responseJson.status === "client")
      {
        SetUrlLocation("clientPage.html");
      }
      else SetUrlLocation("index.html");
    }
    else alert("Такого пользователя не существует!");
  });

  jqXHR.fail(function(data)
  {
    alert("fail");
  })
}

function EqualsClientsObjects(clientObj1, clientObj2)
{
  var result = true;
  for (var key in clientObj1)
  {
    if (clientObj1[key] != clientObj2[key])
    {
      result = false;
      break;
    }
  }
  return result;
}



document.addEventListener("DOMContentLoaded", function()
{
  var form = document.getElementById("loginForm");
  form.addEventListener("submit", function(e)
  {
    e.preventDefault();
    HandleSubmitInForm(form);
  })
});
