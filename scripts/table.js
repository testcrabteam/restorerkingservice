

function UpdateTableContentFromResponseObj(responseJsonObj, table)
{
  var objValues = JSON.parse(responseJsonObj);

  var tbody = table.querySelector("tbody");
  var tablePatternLinks = GetTableRowLinks(table);

  RemoveOldTableContent(tbody);

  AddEveryObjectToTable(objValues, tbody, tablePatternLinks);

  function RemoveOldTableContent(tbody)
  {
    var content = tbody.querySelectorAll("tr:not(:first-child)");
    content.forEach(function(item)
    {
      tbody.removeChild(item);
    })
  }

  function AddEveryObjectToTable(objects, tbody, tablePatternLinks)
  {
    for(var obj of objects)
    {
      var tr = GetTableRowFromObject(obj, tablePatternLinks);
      tbody.appendChild(tr);
    }

    function GetTableRowFromObject(obj, tablePatternLinks)
    {
      var tr = document.createElement("tr");
      var ind = 0;

      for(var field in obj)
      {
        var td = document.createElement("td");

        if (tablePatternLinks[ind++] != undefined)
        {
          var link = GetTableLink(tablePatternLinks[ind - 1]);
          td.appendChild(link);
        }
        else td.textContent = obj[field];

        tr.appendChild(td);
      }
      return tr;

      function GetTableLink(patternLink)
      {
        var href = patternLink.getAttribute("href");
        var link = document.createElement("a");
        link.setAttribute("href", href);
        link.textContent = obj[field];
        return link;
      }
    }
  }
}

function GetTableRowLinks(table)
{
  var tr = table.querySelector("tbody tr:last-child");
  var links = [];

  if (tr != undefined)
  {
    var colums = tr.querySelectorAll("td");
    colums.forEach(function(item, index)
    {
      var a = colums[index].querySelector("a");
      if (a) links[index] = a;
    })
  }

  return links;
}

document.addEventListener("DOMContentLoaded", function()
{
  var tablesForms = document.querySelectorAll(".tableForm");
  tablesForms.forEach(function(table)
  {
    var selects = table.querySelectorAll("select");
    selects.forEach(function(select)
    {
      select.addEventListener("change", function()
      {
        CreateTableContentFromDatabase();
      })
    })
  })
})
// else
// {
//
//   jqXHR = $.get
//   (
//     "php/joinList.php",
//     {'joinObject': joinObject, 'sortField': sortField, 'sortCase', sortCase}
//   );
// }
