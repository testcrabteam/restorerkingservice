function UpdateSessionFromDatabaseById(id)
{
  //Ajax response to OrderToDepartment table
}

function GetInfoFromSession()
{
  // var id = sessionStorage.getItem("id");
  // console.log(id);
  // // var statement = sessionStorage.getItem("statement");
  // // var description = sessionStorage.getItem("description");
  // // var date = sessionStorage.getItem("date");
  // if (id != undefined)
  // {
  //   var parent = document.getElementById("orderDesc");
  //   var idBlock = parent.querySelector("h1");
  //   idBlock.textContent = id;
  //   // var statementBlock = parent.querySelector("h2");
  //   // statementBlock.textContent = statement;
  //   // var descBlock = parent.querySelector("p");
  //   // descBlock.textContent = description;
  //   // var dateBlock = document.querySelector("#rootCalendar");
  //   // dateBlock.textContent = date;
  // }
}

function CreateTableContentFromDatabase()
{
  var table = document.querySelector(".serverTable");
  var page = sessionStorage.getItem("page");
  var id = sessionStorage.getItem("id");
  var idOrder = sessionStorage.getItem("idOrder");

  var sortField = document.getElementById("sortField").value;
  var sortCase = document.getElementById("sortCase").value;

  jqXHR = $.get
  (
    "php/taskOfDepartments.php",
    {
      'id': id,
      "idOrder": idOrder,
      'sortField': sortField,
      'sortCase': sortCase
    }
  );

  jqXHR.done(function(data)
  {
    console.log(data);
    var responseObj = JSON.parse(data);
    var tableDataObj = [];

    for (var i = 0; i < responseObj.length; i++)
    {
      tableDataObj[i] = {};
      tableDataObj[i].id = responseObj[i].id;
      tableDataObj[i].idWorker = responseObj[i].idWorker;
      tableDataObj[i].statement = responseObj[i].statement;
      tableDataObj[i].dateComplete = responseObj[i].dateComplete;
    }
    UpdateTableContentFromResponseObj(JSON.stringify(tableDataObj), table); //table.js
    SetLinkText(); //LinkRequest.js
  });
}

document.addEventListener("DOMContentLoaded", function()
{
  // var id = sessionStorage.getItem("id");
  // UpdateSessionFromDatabaseById(id);
  // GetInfoFromSession();
  CreateTableContentFromDatabase()
})
