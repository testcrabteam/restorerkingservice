function CreateTableContentFromDatabase()
{
  var table = document.querySelector(".serverTable");
  var page = sessionStorage.getItem("page");

  var sortField = document.getElementById("sortField").value;
  var sortCase = document.getElementById("sortCase").value;

  jqXHR = $.get
  (
    "php/mainList.php",
    {'page': page, 'sortField': sortField, 'sortCase': sortCase}
  );

  jqXHR.done(function(data)
  {
    var responseObj = JSON.parse(data);
    var tableDataObj = [];

    for (var i = 0; i < responseObj.length; i++)
    {
      tableDataObj[i] = {};
      tableDataObj[i].id = responseObj[i].id;
      tableDataObj[i].idClient = responseObj[i].idClient;
      tableDataObj[i].clientDate = responseObj[i].clientDate;
      tableDataObj[i].statement = responseObj[i].statement;
    }

    UpdateTableContentFromResponseObj(JSON.stringify(tableDataObj), table); //table.js
    SetLinkText(); //LinkRequest.js
  });
}

document.addEventListener("DOMContentLoaded", function()
{
  CreateTableContentFromDatabase();
})
