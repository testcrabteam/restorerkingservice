function CreateTableContentFromDatabase()
{
  var table = document.querySelector(".serverTable");
  var page = sessionStorage.getItem("page");

  var sortField = document.getElementById("sortField").value;
  var sortCase = document.getElementById("sortCase").value;

  jqXHR = $.get
  (
    "php/mainList.php",
    {'page': page, 'sortField': sortField, 'sortCase': sortCase}
  );

  jqXHR.done(function(data)
  {
    var responseObj = JSON.parse(data);
    var tableDataObj = [];

    for (var i = 0; i < responseObj.length; i++)
    {
      tableDataObj[i] = {};
      tableDataObj[i].name = responseObj[i].surname + " "
          + responseObj[i].name + " " + responseObj[i].middleName;
      tableDataObj[i].email = responseObj[i].email;
      tableDataObj[i].regDate = responseObj[i].regDate;
    }

    UpdateTableContentFromResponseObj(JSON.stringify(tableDataObj), table); //table.js
    SetLinkText(); //LinkRequest.js
  });
}

document.addEventListener("DOMContentLoaded", function()
{
  CreateTableContentFromDatabase();
})
