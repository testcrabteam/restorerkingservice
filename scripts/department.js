function CreateTableContentFromDatabase()
{
  var table = document.querySelector(".serverTable");
  var page = sessionStorage.getItem("page");
  var id = sessionStorage.getItem("id");

  var sortField = document.getElementById("sortField").value;
  var sortCase = document.getElementById("sortCase").value;

  var joinObject =
  {
    source: "order",
    destination: "ordersWithDepartments",
    value: id
  };
  var jsonJoinObj = JSON.stringify(joinObject);

  jqXHR = $.get
  (
    "php/muchToMuchList.php",
    {'joinObject': jsonJoinObj, 'sortField': sortField, 'sortCase': sortCase}
  );

  jqXHR.done(function(data)
  {
    var responseObj = JSON.parse(data);
    var tableDataObj = [];

    for (var i = 0; i < responseObj.length; i++)
    {
      tableDataObj[i] = {};
      tableDataObj[i].id = responseObj[i].id;
      tableDataObj[i].idClient = responseObj[i].idClient;
      tableDataObj[i].clientDate = responseObj[i].clientDate;
      tableDataObj[i].statement = responseObj[i].statement;
    }

    UpdateTableContentFromResponseObj(JSON.stringify(tableDataObj), table); //table.js
    SetLinkText(); //LinkRequest.js
  });
}

function SetSessionInfoFromDatabase()
{
  var page = sessionStorage.getItem("page");
  var id = sessionStorage.getItem("id");

  var jqXHR = $.get
  (
    "php/selectQueryCreator.php",
    {'id': id, 'page': page}
  );

  jqXHR.done(function(data)
  {
    var info = JSON.parse(data);
    if (info != undefined)
    {
      sessionStorage.setItem("statement", info.statement);
      sessionStorage.setItem("description", info.description);
      sessionStorage.setItem("name", info.name);
    }
  })
}

function GetInfoFromSession()
{
  var id = sessionStorage.getItem("id");
  var statement = sessionStorage.getItem("statement");
  var description = sessionStorage.getItem("description");
  var name = sessionStorage.getItem("name");

  if (id != undefined)
  {
    var parent = document.getElementById("orderDesc");
    var idBlock = parent.querySelector("h1");
    idBlock.textContent = "Отдел №" + id;
    var nameBlock = parent.querySelector("h2");
    nameBlock.textContent = name;
    var descBlock = parent.querySelector("p");
    descBlock.textContent = description;
  }
}

document.addEventListener("DOMContentLoaded", function()
{
  SetSessionInfoFromDatabase();
  GetInfoFromSession();
  CreateTableContentFromDatabase();
})
