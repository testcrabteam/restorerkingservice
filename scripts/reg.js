function HandleSubmitInForm(form)
{
  var name = form.querySelector('input[name = "name"]').value;
  var email = form.querySelector('input[name = "email"]').value;
  var passwd = form.querySelector('input[name = "password"]').value;

  var jqXHR = $.post
  (
    "php/reg.php",
    {'name': name, 'email': email, 'password': passwd}
  );

  jqXHR.done(function(data)
  {
    if (data)
    {
      sessionStorage.setItem("status", "client");
      sessionStorage.setItem("clientId", data);
      SetUrlLocation("clientPage.html");
    }
    else alert("Unsuccessful registration!");
  });

  jqXHR.fail(function(data)
  {
    alert("Undefined error!");
  })
}

document.addEventListener("DOMContentLoaded", function()
{
  var form = document.getElementById("form");
  form.addEventListener("submit", function(e)
  {
    e.preventDefault();
    HandleSubmitInForm(form);
  })
});
