//Use only with
//linksLibrary.js

function EditOrder(e, calendar)
{
  var link = e.currentTarget.getAttribute("href");
  var orderInfo = {};
  var parent = document.getElementById("orderDesc");
  var description = parent.querySelector("p");

  orderInfo.date = new Date();
  orderInfo.date.setDate(calendar.expectedDay);
  orderInfo.date.setMonth(calendar.expectedMonth - 1);
  orderInfo.date.setFullYear(calendar.expectedYear);
  orderInfo.text = description.textContent;
  orderInfo.id = sessionStorage.getItem("id");
  sessionStorage.setItem("orderInfo", JSON.stringify(orderInfo));

  SetUrlLocation(link);
}

function SplitClientAndAdminSystem()
{
  var status = sessionStorage.getItem("status");

  var parent = document.getElementById("orderDesc");

  switch(status)
  {
    case "client":
      var formButton = document.querySelector(".formButton");
      formButton.addEventListener("click", function(e)
      {
        e.preventDefault();
        EditOrder(e, calendar);
      });
      var form = parent.querySelector("form");
      parent.removeChild(form);
    break;

    case "worker":
      var link = parent.querySelector("a.formButton");
      parent.removeChild(link);

      var taskListBlock = document.getElementById("tasksList");
      taskListBlock.style.display = "block";
    break;

    default: break;
  }
}

function CreateTableContentFromDatabase()
{
  var table = document.querySelector(".serverTable");
  var page = sessionStorage.getItem("page");
  var id = sessionStorage.getItem("idOrder");

  var sortField = document.getElementById("sortField").value;
  var sortCase = document.getElementById("sortCase").value;

  var joinObject =
  {
    source: "department",
    destination: "ordersWithDepartments",
    value: id
  };
  var jsonJoinObj = JSON.stringify(joinObject);

  jqXHR = $.get
  (
    "php/muchToMuchList.php",
    {'joinObject': jsonJoinObj, 'sortField': sortField, 'sortCase': sortCase}
  );

  jqXHR.done(function(data)
  {
    var responseObj = JSON.parse(data);
    var tableDataObj = [];

    for (var i = 0; i < responseObj.length; i++)
    {
      tableDataObj[i] = {};
      tableDataObj[i].id = responseObj[i].id;
      tableDataObj[i].name = responseObj[i].name;
    }

    UpdateTableContentFromResponseObj(JSON.stringify(tableDataObj), table); //table.js
    SetLinkText(); //LinkRequest.js
  });

  jqXHR.fail(function(data)
  {
    alert("Fail");
  });
}

document.addEventListener("DOMContentLoaded", function()
{
  setTimeout(function()
  {
    var id = sessionStorage.getItem("id");
    sessionStorage.setItem("idOrder", id);
    sessionStorage.setItem("nameDescription", "Заказ");
    SplitClientAndAdminSystem();
    CreateTableContentFromDatabase();
  })
})
