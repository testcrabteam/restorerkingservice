function UpdateSessionFromDatabaseByEmail(email)
{
  var page = "clientPage";
  var jqXHR = $.get
  (
    "php/selectQueryCreator.php",
    {'page': page, 'id': email}
  );

  jqXHR.done(function(data)
  {
    if (data)
    {
      var info = JSON.parse(data);
      var fullName = info.surname + " " + info.name + " " + info.middleName;
      sessionStorage.setItem("name", fullName);
      sessionStorage.setItem("date", info.regDate);
      sessionStorage.setItem("idClient", info.id);
      UpdateInfoFromSession();
    }
    else alert("Undefined data!");
  });
  jqXHR.fail(function(data)
  {
    alert("Request is failed!");
    SetUrlLocation("index.html")
  })
}

function UpdateInfoFromSession()
{
  var name = sessionStorage.getItem("name");
  var status = sessionStorage.getItem("status");
  var email = (status === "client")
              ? sessionStorage.getItem("clientId")
              : sessionStorage.getItem("id");
  var date = sessionStorage.getItem("date");
  if (name != undefined)
  {
    var parents = document.querySelectorAll(".feedback-info-block");
    if (parents != undefined)
    {
      UpdatePageContent(parents[0], name);
      UpdatePageContent(parents[1], date);
      UpdatePageContent(parents[2], email);
    }
  }
}

function UpdatePageContent(parent, textValue)
{
  if (parent != undefined && textValue != undefined)
  {
    var block = parent.querySelector("p");
    block.textContent = textValue;
  }
}

function WorkerPageHandling()
{
  var status = sessionStorage.getItem("status");
  if (status !== "client")
  {
    HideTableFormButtons();
    EditFormHeaderText();
  }
}

function HideTableFormButtons()
{
  var buttonsParent = document.querySelector(".feedback-form > .container");
  buttonsParent.style.display = "none";
}

function EditFormHeaderText()
{
  var head = document.querySelector(".feedback-form > h3:first-child");
  head.textContent = "Заказы клиента"
}

function CreateTableContentFromDatabase()
{
  var table = document.querySelector(".serverTable");
  var page = sessionStorage.getItem("page");
  var id = sessionStorage.getItem("idClient");

  var sortField = document.getElementById("sortField").value;
  var sortCase = document.getElementById("sortCase").value;

  var joinObject =
  {
    source: "client",
    destination: "order",
    value: id
  };
  var jsonJoinObj = JSON.stringify(joinObject);

  jqXHR = $.get
  (
    "php/joinList.php",
    {'joinObject': jsonJoinObj, 'sortField': sortField, 'sortCase': sortCase}
  );

  jqXHR.done(function(data)
  {
    var responseObj = JSON.parse(data);
    var tableDataObj = [];

    for (var i = 0; i < responseObj.length; i++)
    {
      tableDataObj[i] = {};
      tableDataObj[i].id = responseObj[i].id;
      tableDataObj[i].clientDate = responseObj[i].clientDate;
      tableDataObj[i].statement = responseObj[i].statement;
    }

    UpdateTableContentFromResponseObj(JSON.stringify(tableDataObj), table); //table.js
    SetLinkText(); //LinkRequest.js
  });
}

document.addEventListener("DOMContentLoaded", function()
{
  var status = sessionStorage.getItem("status");
  var email = (status === "client")
              ? sessionStorage.getItem("clientId")
              : sessionStorage.getItem("id");
  UpdateSessionFromDatabaseByEmail(email);
  WorkerPageHandling();
  CreateTableContentFromDatabase();
})
