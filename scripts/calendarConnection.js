
var calendar; //External global object of current calendar
function CalendarConnect(dateInfo)
{
  var calendarParent = document.querySelector("#rootCalendar");
  calendar = new HtmlCalendar(calendarParent, dateInfo);
  calendar.render();
}

document.addEventListener("DOMContentLoaded", function()
{
  setTimeout(function()
  {
    var dateStr = sessionStorage.getItem("date");
    var date = new Date(dateStr);

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    var dateInfo = new DateInfo(day, month, year);
    CalendarConnect(dateInfo);
  }, 400)
})
