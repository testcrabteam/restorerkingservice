

function SetUrlLocation(pageName)
{
  var location = window.location.href;
  var splitLocation = location.split("/");
  splitLocation[splitLocation.length - 1] = pageName;
  window.location.href = splitLocation.join("/");
}
